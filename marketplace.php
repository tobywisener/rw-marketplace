<?php
/*
Plugin Name: RaffleWise Marketplace
Plugin URI: https://wisener.me
Description: Allows administration of RaffleWise Marketplace
Version: 1.4
Author: Toby Wisener
Author URI: https://rafflewise.ie

* WC requires at least: 2.2

 * WC tested up to: 3.8

Plugin developed by Toby Wisener (Wisener Solutions Limited) 2020

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

*/

if ( is_readable( __DIR__ . '/vendor/autoload.php' ) ) {
    require __DIR__ . '/vendor/autoload.php';
}

use GuzzleHttp\Client;
use GuzzleHttp\Common\Exception\MultiTransferException;

register_activation_hook(__FILE__, 'rw_marketplace_install_plugin');
function rw_marketplace_install_plugin() {
	global $wpdb;

	$migrate_from_wpraffle = false;

    // Sites table
    $sites_table_name = $wpdb->prefix . 'raffle_wise_marketplace_sites';
    if($wpdb->get_var("show tables like '$sites_table_name'") != $sites_table_name)
    {
        $sql = "CREATE TABLE $sites_table_name
                    ( `id` INT NOT NULL AUTO_INCREMENT ,
                    `site_id` VARCHAR(255) NOT NULL ,
                    `site_name` VARCHAR(100) NOT NULL ,
                    `site_icon` VARCHAR(2083) NULL ,
                    `site_logo` VARCHAR(2083) NULL ,
                    PRIMARY KEY (`id`),
                    UNIQUE (`site_id`));";

        $wpdb->query($sql);
    }

    // Competitions table
	$competitions_table_name = $wpdb->prefix . 'raffle_wise_marketplace_competitions';
	if($wpdb->get_var("show tables like '$competitions_table_name'") != $competitions_table_name)
	{
		$sql = "CREATE TABLE $competitions_table_name
            		( `id` INT NOT NULL AUTO_INCREMENT ,
                    `product_id` BIGINT(20) NOT NULL ,
            		`site_id` VARCHAR(255) NOT NULL ,
            		`name` VARCHAR(100) NOT NULL ,
                    `max_tickets` INT NOT NULL ,
                    `sold_tickets` INT NOT NULL ,
                    `start_datetime` DATETIME NOT NULL ,
            		`end_datetime` DATETIME NOT NULL ,
            		`featured_until` DATETIME NULL ,
            		`ticket_price` DECIMAL(10,2) NOT NULL ,
            		`image_uri` VARCHAR(2083) NULL ,
            		PRIMARY KEY (`id`),
            		INDEX( `product_id`, `site_id`, `featured_until`));";

		$wpdb->query($sql);
	}

	// Customers table
	$customers_tavle_name = $wpdb->prefix . 'raffle_wise_marketplace_customers';
	if($wpdb->get_var("show tables like '$competitions_table_name'") != $competitions_table_name)
    {
        $sql = "CREATE TABLE `wordpress`.`wp_raffle_wise_marketplace_customers`
                    ( `id` INT NOT NULL AUTO_INCREMENT ,
                    `competition_id` INT NOT NULL ,
                    `site_id` INT NOT NULL ,
                    `ip_address` INT (4) NOT NULL ,
                    `visited_at` DATETIME NOT NULL ,
                    `ordered_at` DATETIME NULL ,
                    `order_value` DECIMAL(10,2) NULL ,
                    `user_id` INT NULL ,
                    PRIMARY KEY (`id`),
                    INDEX `competition_id` (`competition_id`),
                    INDEX `site_id` (`site_id`),
                    INDEX `user_id` (`user_id`));";

        $wpdb->query($sql);
    }

}

class RaffleWiseMarketplace {

	// The complete slug for this plugin
	private $plugin_slug = "raffle-wise-market-place";

    private $db_sites_table_name = /* WPDB Prefix */ "raffle_wise_marketplace_sites";

	private $db_competitions_table_name = /* WPDB Prefix */ "raffle_wise_marketplace_competitions";

	private $db_customers_table_name = /* WPDB Prefix */ "raffle_wise_marketplace_customers";

	function __construct() {
		global $wpdb;

        $this->db_sites_table_name = $wpdb->prefix . $this->db_sites_table_name;

		$this->db_competitions_table_name = $wpdb->prefix . $this->db_competitions_table_name;

		$this->db_customers_table_name = $wpdb->prefix . $this->db_customers_table_name;
	}

	// Attach all hooks and filters
	function run() {

		add_action( 'admin_enqueue_scripts', array($this, 'enqueue_admin_scripts') );

		// Enqueue the timer JS
		add_action('wp_enqueue_scripts',array($this, 'enqueue_scripts'));

		// Add the options to the menu
		add_action( 'admin_menu', array($this, 'wooraffle_menu') );

        // Shortcode for displaying competitions
        add_shortcode( 'rw-marketplace-competitions', [$this,'shortcode_competitions'] );

        // Shortcode for competition filters
        add_shortcode( 'rw-marketplace-competition-filters', [$this, 'shortcode_competition_filters'] );

        // Shortcode for slider carousel
        add_shortcode( 'rw-marketplace-carousel', [$this,'shortcode_carousel'] );

        // Shortcode for calendar
        add_shortcode( 'rw-marketplace-calendar', [$this,'shortcode_calendar'] );

        // Shortcode for site icons
        add_shortcode( 'rw-marketplace-site-icons', [$this,'shortcode_site_icons'] );

		add_action( 'rest_api_init', function() {

			register_rest_route( // Resource for updating a competition resource
		    	'raffle-wise-marketplace/v1', '/competition/tickets',
		    	[ 'methods' => 'PUT', 'callback' => [$this, 'api_update_stock'], 'permission_callback' => '__return_true' ]
			);

			register_rest_route( // Resource for getting grouped competition end dates
                'raffle-wise-marketplace/v1', '/competitions/end-dates',
                [ 'methods' => 'GET', 'callback' => [$this, 'db_get_competition_dates'], 'permission_callback' => '__return_true' ]
            );

            register_rest_route( // Resource for reconciling a customer record with an order
                'raffle-wise-marketplace/v1', '/customers',
                [ 'methods' => 'PUT', 'callback' => [$this, 'api_update_customer'], 'permission_callback' => '__return_true' ]
            );

		});

	}

	public function render_settings_field($args) {
       if($args['wp_data'] == 'option'){
            $wp_data_value = get_option($args['name']);
        } elseif($args['wp_data'] == 'post_meta'){
            $wp_data_value = get_post_meta($args['post_id'], $args['name'], true );
        }

        switch ($args['type']) {

            case 'input':
                $value = ($args['value_type'] == 'serialized') ? serialize($wp_data_value) : $wp_data_value;

                switch($args['subtype']) {

                    case 'checkbox':
                        $checked = ($value) ? 'checked' : '';

                        echo '<input type="'.$args['subtype'].'" id="'.$args['id'].'" "'.$args['required'].'" name="'.$args['name'].'" size="40" value="1" '.$checked.' />';
                    break;

                    case 'color':

                        if($this->wp_color_picker) {
                            // Reuse wordpress's built in color picker
                            echo '<div class="farb-popup-wrapper">';

                            echo '<input type="text" id="'.$args['id'].'" name="'.$args['name'].'" value="'.$value.'" class="popup-colorpicker" style="width:100px;"/>';

                            echo '<div id="'.$args['id'].'picker" class="color-picker"></div>';

                            echo '</div>';
                        } else {
                            // Use the default HTML5 colour input
                            echo '<input type="color" id="'.$args['id'].'" name="'.$args['name'].'" value="'.$value.'" class="popup-colorpicker" style="width:70px;"/>';

                        }

                    break;

                    case 'textarea':
                        echo '<textarea id="'.$args['id'].'" name="'.$args['name'].'" style="min-width:300px;">'.$value.'</textarea>';
                    break;

                    default: /* Any other type of input, probably text */
                        $prependStart = (isset($args['prepend_value'])) ? '<div class="input-prepend"> <span class="add-on">'.$args['prepend_value'].'</span>' : '';
                        $prependEnd = (isset($args['prepend_value'])) ? '</div>' : '';
                        $step = (isset($args['step'])) ? 'step="'.$args['step'].'"' : '';
                        $min = (isset($args['min'])) ? 'min="'.$args['min'].'"' : '';
                        $max = (isset($args['max'])) ? 'max="'.$args['max'].'"' : '';
                        if(isset($args['disabled'])){
                            // hide the actual input bc if it was just a disabled input the informaiton saved in the database would be wrong - bc it would pass empty values and wipe the actual information
                            echo $prependStart.'<input type="'.$args['subtype'].'" id="'.$args['id'].'_disabled" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'_disabled" size="40" disabled value="' . esc_attr($value) . '" /><input type="hidden" id="'.$args['id'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />'.$prependEnd;
                        } else {
                            echo $prependStart.'<input type="'.$args['subtype'].'" id="'.$args['id'].'" "'.$args['required'].'" '.$step.' '.$max.' '.$min.' name="'.$args['name'].'" size="40" value="' . esc_attr($value) . '" />'.$prependEnd;
                        }
                        /*<input required="required" '.$disabled.' type="number" step="any" id="'.$this->plugin_name.'_cost2" name="'.$this->plugin_name.'_cost2" value="' . esc_attr( $cost ) . '" size="25" /><input type="hidden" id="'.$this->plugin_name.'_cost" step="any" name="'.$this->plugin_name.'_cost" value="' . esc_attr( $cost ) . '" />*/
                    break;
                }

                break;
            default:
                // code...
                break;
        }

        echo '<label for="'.$args['id'].'" class="raffle_wise_label">'.$args['desc'].'</label>';
    }

    /**
     * Enqueue the admin scripts
     */
    function enqueue_admin_scripts() {
    }

    /**
     * Enqueue the public scripts
     */
    function enqueue_scripts() {
        wp_enqueue_script( 'rw-marketplace',
            plugin_dir_url( __FILE__ ) . '/js/rw-marketplace.js',
            array( 'jquery' ),
            '1.0.0', true );

        wp_enqueue_style( 'rw-marketplace-style',
            plugin_dir_url( __FILE__ ) . '/css/rw-marketplace.css' );
    }

    function wooraffle_menu()
    {
        add_menu_page(
            __('RaffleWise Marketplace','rafflewise-marketplace'),
            __('RaffleWise Marketplace','rafflewise-marketplace'),
            'manage_options',
            'rafflewise-marketplace',
            array($this, 'admin_page'),
            plugin_dir_url( __FILE__ ) . 'img/rw_icon.png',
            1
        );
    }

    /**
     * Produces a calendar of upcoming competitions
     *
     * [rw-marketplace-calendar]
     */
    function shortcode_calendar( $atts ) {
        $competition_ending_dates = $this->db_get_competition_dates();

        return '
        <script type="text/javascript">
        var RW_MARKETPLACE_ENDING_DATES = '.json_encode($competition_ending_dates).';
        </script>
        <div class="calendar-wrapper rw-calendar">
                  <button id="btnPrev" type="button">Prev</button>
                      <button id="btnNext" type="button">Next</button>
                  <div id="divCal"></div>
                </div>';
    }

    /**
     * Produces a list of site icons
     *
     * [rw-marketplace-site-icons]
     */
     function shortcode_site_icons( $atts ) {
        $rethtml = '<ul class="rw-marketplace-site-icons">';

        $sites = $this->db_get_sites();

        foreach ($sites as $site) {
            if (strlen($site['site_icon']) == 0) {
                // This site doesn't have a site icon defined, don't display it
                continue;
            }

            $rethtml .= '<li>
                            <a href="'.$this->get_site_link($site).'" target="_blank" title="'.$site['site_name'].' (New tab)">
                                <img src="'.$site['site_icon'].'" width="27" height="27"/>
                            </a>
                         </li>';
        }

        $rethtml .= '</ul>';

        return $rethtml;
     }

    /**
     * Processes a competition site, deciding to add or update each one.
     *
     * [rw-marketplace-competitions]
     */
    function shortcode_competitions( $atts ) {
        $rethtml = '';

        $rethtml = '
            <div class="">
                <ul class="rw-marketplace-competitions products columns-4">';

        $competitions = $this->db_get_competitions_query([
            "order_by" => $atts['order_by'] ?? $_GET['order_by'] ?? 0,
            "limit" => $atts['limit'] ?? 0
        ]);

        foreach($competitions as $competition) {
            $rethtml .= '
                     <li class="rw-marketplac-competition">
                        <a href="/view-competition?cid=' . $competition['id'] . '" class="woocommerce-LoopProduct-link woocommerce-loop-product__link">
                            <div class="rw-marketplace-img" style="background-image: url('.$this->get_competition_image_url($competition).');">
                                <div class="rw-site-icon" title="' . $competition['site_name'] . '" style="background-image: url('.$competition['site_icon'].')"> </div>
                            </div>
                            <div id="countdown_196_wrapper" class="rafflewise_progress_wrapper error" title="Competition ends 04/16/2021 18:00:00" data-timeout="' . $competition['end_datetime'] . '" data-timestamp-start="1614643200" data-timestamp-end="1618592400">
                                <i class="fas fa-clock" style="color: #ffffff;" aria-hidden="true"></i>
                                <div id="countdown_196" class="rafflewise_progress_fill" style="width: 118%; background: #ffffff;"></div>
                                <span>EXPIRED</span>
                            </div>
                            <h5 class="woocommerce-loop-product__title" title="' . $competition['name'] . '">' . $competition['name'] . '</h5>
                            <bdi>
                                <span class="woocommerce-Price-currencySymbol">£</span>' . $competition['ticket_price'] . '
                            </bdi>
                     </a>
                     <a class="button" href="/view-competition?cid=' . $competition['id'] . '">Enter</a>
                     </li>
                     ';
        }


        $rethtml .= '</ul>
                     </div>';

        return $rethtml;
    }

    /**
     * Provides filtering controls for the competition grid.
     *
     * [rw-marketplace-competition-filters]
     */
    function shortcode_competition_filters() {
        $rethtml = '
        <form method="GET" action="/competitions">
        <select name="percent_sold">
            <option value="" selected>% Tickets sold</option>
            <option value="5">5% or less</option>
            <option value="10">10% or less</option>
            <option value="20">20% or less</option>
            <option value="40">40% or less</option>
            <option value="50">50% or less</option>
            <option value="75">75% or less</option>
            <option value="90">90% or less</option>
        </select>
        <hr/>
        <input type="submit" name="submit" value="Update"/>
        </form>
        ';

        return $rethtml;
    }

    function shortcode_carousel() {

        $competitions = $this->db_get_competitions_query();

        $rethtml = '
            <h3 class="post-header">Featured Competitions</h3>
            <div id="rw-carousel-widget" class="widget buddypress widget">
        		<div class="rw-carousel js-flickity" data-flickity-options=\'{ "wrapAround": true }\'>';

        foreach ($competitions as $competition) {
            $rethtml .= '<div class="gallery-cell" style="background-image: url('.$this->get_competition_image_url($competition).')">
                             <a href="/view-competition?cid=' . $competition['id'] . '"><h1>' . $competition['name'] . '</h1></a>
                             <!-- Timer here -->
                         </div>';
        }

        $rethtml .= '
                  </div>
                  </div>';

        return $rethtml;
    }

    /**
     * Is HTTPS?
     *
     * Determines if the application is accessed via an encrypted
     * (HTTPS) connection.
     *
     * @return  bool
     */
    function is_https()
    {
        if ( ! empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off')
        {
            return TRUE;
        }
        elseif (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) === 'https')
        {
            return TRUE;
        }
        elseif ( ! empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off')
        {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * Returns a direct link to a competition's image
     */
    function get_competition_image_url($competition) {
        $protocol = $this->is_https() ? 'https://' : 'http://';
        return  $protocol . $competition['site_id'] . '/wp-content/uploads/' . $competition['image_uri'];
    }

    /**
     * Returns a direct link to the competitions site
     * $entity can be a site or a competition (anything with ['site_id'] property)
     *
     * Return value example: https://rafflewise.ie/
     */
    function get_site_link($entity) {
        $protocol = $this->is_https() ? 'https://' : 'http://';

        return $protocol . $entity['site_id'] . '/';
    }

    /**
     * Returns a direct link to the competitions page
     */
    public function get_competition_page_link($competition) {
        return $this->get_site_link($competition) . '?p='.$competition['product_id'];
    }

	function admin_page() {
        global $wpdb;

		echo '<h1>Marketplace</h1>';

		if (isset($_POST['site_id'])) {
            // The admin has submitted a site to process
            $this->scan_site($_POST['site_id']);
		}

		if (isset($_GET['scan_all'])) {
		    // The site admin wants to scan all known sites
            $sites = $this->db_get_sites();

            foreach ($sites as $site) {
                $this->scan_site($site['site_id']);
            }
		}
        ?>
        <form action="/wp-admin/admin.php?page=rafflewise-marketplace" method="POST">
            <input type="text" name="site_id"/>
            <input type="submit" value="Add Site"/>
        </form>
        <?php
	}

    /**
     * Scans a site for new/updated competitions.
     */
    function scan_site($site_id, $dry_run = FALSE) {
        // Ensure the protocol is specified
        if (!$this->str_starts_with($site_id, 'http')) {
            $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';

            $site_id = $protocol . $site_id;

            $this->debug_log('Appending protocol - site id now: ' . $site_id);
        }

        // Create the Guzzle client
        $client = new Client([ 'base_uri' => $site_id ]);

        try {
            // Add or update this site
            $site_info = json_decode($client->request('GET', '/wp-json/raffle-wise/v1/site-info')->getBody()->getContents(), true);
            $this->debug_log('Site info received:', $site_info);

            if (!$dry_run) {
                $site_result = $this->add_update_site($site_info);
                $this->debug_log('Add/update site result:', $site_result);
            }

            // Add or update the competitions listed for this site
            $competitions = json_decode($client->request('GET', '/wp-json/raffle-wise/v1/competitions')->getBody()->getContents(), true);
            $this->debug_log('Competitions received:', $competitions);

            if (!$dry_run) {
                $competitions_result = $this->add_update_competitions($site_info, $competitions);
                $this->debug_log('Add/update competitions result:', $competitions_result);
            }

        } catch (Exception $e) {
            echo 'The following exceptions were encountered:' . PHP_EOL;
            print_r($e);
        }
    }

    /**
     * Processes a competition site, deciding to add or update each one.
     */
    function add_update_site($site_info) {
        global $wpdb;
        $retval = false;

        $current_site = $this->db_get_site($site_info['site_id']);

        $this->debug_log("Current site: ", $current_site);
        $this->debug_log("Site info: ", $site_info);
       if ($current_site == NULL) {
            $this->debug_log('Adding new site...');
            $retval = $this->db_insert_site($site_info);
        } else /* Site exists, update */ {
            $this->debug_log('Updating site...');
            $retval = $this->db_update_site($site_info);
            $this->debug_log($wpdb->last_query);
        }

        if ($wpdb->last_error) {
          $this->debug_log('MySQL error adding/updating site:',$wpdb->last_error);
        }

        return $retval;
    }

    /**
     * Processes an array of competitions, deciding to add or update each one.
     */
    function add_update_competitions($site_info, array $competitions) {
        global $wpdb;
        $retval = [0,0];

        // Get all current competitions in the database for this site
        $site_id = $site_info['site_id'];
        $current_competitions = $this->db_get_competitions($site_id);
        $current_product_ids = array_column($current_competitions, 'product_id');

        $this->debug_log('Current product ids:',$current_product_ids);

        foreach ($competitions as $competition) {
            // Check if competition exists
            if (in_array($competition['product_id'], $current_product_ids)) {
                $this->debug_log('Updating competition',$competition['product_id']);
                // Competition already exists, update
                $retval[1] = $retval[1] + $this->db_update_competition($site_id, $competition);
                $this->debug_log($wpdb->last_query);
            } else /* Competition doesn't exist, insert */ {
                $this->debug_log('Inserting competition',$competition['product_id']);
                $retval[0] = $retval[0] + $this->db_insert_competition($site_id, $competition);
            }

            if ($wpdb->last_error) {
                $this->debug_log('MySQL error adding/updating competition:',$wpdb->last_error);
            }
        }


        if (count($competitions) == 0 && count($current_competitions) > 0) {
            // No competitions to add or update
            return $retval;
        }

        return $retval;
    }

    /**
     * Updates a customer record to indicate they have placed an order
     */
    function api_update_customer($request) {
        $customer_id = $request['rwmktoken'];
        $customer = $this->db_get_customer($customer_id);

        if($customer == NULL || $customer['ordered_at'] != NULL) {
            // Don't update the ordered_at twice, for example if order gets reset to Pending Payment by admin
            return;
        }

        $rowsUpdated = $this->db_update_customer([
            'id' => $customer_id,
            'ordered_at' => wp_date('Y-m-d H:i:s'),
            'order_value' => $request['order_value']
        ]);
    }

    /**
     * Inserts a site based on some site info
     */
    function db_insert_site($site_info) {
        global $wpdb;

        return $wpdb->insert($this->db_sites_table_name,[
           "site_id" => $site_info["site_id"],
           "site_name" => $site_info["site_name"],
           "site_icon" => $site_info["site_icon"]
        ],['%s','%s','%s']);
    }

    /**
     * Gets a single customer by its id
     */
    public function db_get_customer($customer_id) {
        global $wpdb;

        $sql = "SELECT
            *
            FROM
            ".$this->db_customers_table_name." as customers
            WHERE customers.id='".$customer_id."'";

        $results = $wpdb->get_results($sql, 'ARRAY_A');

        return reset($results);
    }

    /**
     * Gets a single site by its site_id
     */
    public function db_get_site($site_id) {
        global $wpdb;

        $sql = "SELECT
            *
            FROM
            ".$this->db_sites_table_name." as sites
            WHERE sites.site_id='".$site_id."' LIMIT 1";

        $results = $wpdb->get_results($sql, 'ARRAY_A');

        return reset($results);
    }

    /**
     * Gets all sites currently on the database
     */
    public function db_get_sites() {
        global $wpdb;

        $sql = "SELECT
            *
            FROM
            ".$this->db_sites_table_name." as sites";

        $results = $wpdb->get_results($sql, 'ARRAY_A');

        return $results;
    }

    /**
     * Updates a site's record based on site info
     */
    function db_update_site($site_info) {
        global $wpdb;

        return $wpdb->update( $this->db_sites_table_name, [
          "site_name" => $site_info["site_name"],
          "site_icon" => $site_info["site_icon"]
        ], ['site_id' => $site_info['site_id']],
        "%s",
        array("%s")
        );
    }

    /**
     * Gets a competition by its id
     */
    public function db_get_competition($competition_id) {
        global $wpdb;

        $sql = "SELECT
            *
            FROM
            ".$this->db_competitions_table_name." as competitions
            WHERE competitions.id='".$competition_id."' LIMIT 1";

        $results = $wpdb->get_results($sql, 'ARRAY_A');

        return reset($results);
    }

    /**
     * Inserts a competition based on an array
     */
    function db_insert_competition($site_id, $competition) {
        global $wpdb;

        return $wpdb->insert($this->db_competitions_table_name,[
            'product_id' => $competition['product_id'],
            'site_id' => $site_id,
            'name' => $competition['name'],
            'max_tickets' => $competition['max_tickets'],
            'sold_tickets' => $competition['sold_tickets'],
            'start_datetime' => $this->format_mysql_date($competition['start_datetime']),
            'end_datetime' => $this->format_mysql_date($competition['end_datetime']),
            'ticket_price' => $competition['ticket_price'],
            'image_uri' => $competition['image_uri']
        ],
        ['%s','%s','%s','%d','%d','%s', '%s', '%f', '%s']);
    }

    /**
     * Inserts a customer based on an array
     *
     * Returns the ID of the newly inserted customer record
     */
    public function db_insert_customer($customer) {
        global $wpdb;

        // Serialise the IP address to be stored in INT(4)
        $customer['ip_address'] = ip2long($customer['ip_address']);

        $wpdb->insert($this->db_customers_table_name,$customer);

        if($wpdb->last_error !== '') {
            throw new Exception($wpdb->last_error);
        }

        return $wpdb->insert_id;
    }

    /**
     * Updates a customer record
     */
    function db_update_customer($customer) {
        global $wpdb;

        if(isset($customer['ip_address'])) {
            // Serialise the IP address to be stored in INT(4)
            $customer['ip_address'] = ip2long($customer['ip_address']);
        }

        return $wpdb->update( $this->db_customers_table_name, $customer, ['id' => $customer['id']]);
    }

    /**
     * Updates a competition based on an array
     */
    function db_update_competition($site_id, $competition) {
        global $wpdb;

        $wpdb->query('UPDATE '.$this->db_competitions_table_name.' SET
            name = "'.$competition['name'].'",
            max_tickets = '.$competition['max_tickets'].',
            sold_tickets = '.$competition['sold_tickets'].',
            start_datetime = "'.$this->format_mysql_date($competition['start_datetime']).'",
            end_datetime = "'.$this->format_mysql_date($competition['end_datetime']).'",
            ticket_price = '.$competition['ticket_price'].',
            image_uri = "'.$competition['image_uri'].'"
            WHERE site_id LIKE "'.$site_id.'" AND product_id = '.$competition['product_id'].' LIMIT 1');

        return intval($wpdb->last_result);
    }

    /**
     * Gets an array of competitions for a given site id
     */
    function db_get_competitions($site_id = NULL) {
        global $wpdb;

        $sql = "SELECT
            *
            FROM
            ".$this->db_competitions_table_name." as competitions";

        if ($site_id != NULL) {
            $sql .= ' WHERE competitions.site_id="'.$site_id.'"';
        }

        return $wpdb->get_results($sql, 'ARRAY_A');
    }

    /**
     * Gets an array of competitions, taking the query parameter filters into account
     */
    function db_get_competitions_query($query = []) {
        global $wpdb;

        $orderby = $query['order_by'] ?? 0;

        $sql = "SELECT
            competitions.*,
            COUNT(visitors.id) AS visitor_count,
            sites.*
            FROM
            ".$this->db_competitions_table_name." AS competitions
            LEFT JOIN ".$this->db_customers_table_name." AS visitors
                ON competitions.id = visitors.competition_id
            LEFT JOIN ".$this->db_sites_table_name." AS sites
                ON sites.site_id = competitions.site_id";

        // Initial WHERE clause
        $sql .= " WHERE competitions.end_datetime > NOW()";

        if(isset($_GET['ending_on'])) {
            $ending_on = esc_sql( $_GET['ending_on'] );
            $sql .= " AND competitions.end_datetime >= '$ending_on 00:00:00'
                      AND competitions.end_datetime <= '$ending_on 23:59:59'";
        }

        if(isset($_GET['percent_sold'])) {
            $percent_sold = intval(esc_sql( $_GET['percent_sold'] ));
            $sql .= " AND competitions.sold_tickets < (competitions.max_tickets/100*$percent_sold)";
        }

        if(isset($_GET['min_ticket_price'])) {
            $min_ticket_price = esc_sql( $_GET['min_ticket_price'] );
            $sql .= " AND competitions.ticket_price >= '$min_ticket_price'";
        }

        if(isset($_GET['max_ticket_price'])) {
            $max_ticket_price = esc_sql( $_GET['max_ticket_price'] );
            $sql .= " AND competitions.ticket_price <= '$max_ticket_price'";
        }

        // GROUP BY
        $sql .= " GROUP BY competitions.id";

        switch ($orderby) {
            case 1 /* Most views */:
                $sql .= ' ORDER BY visitor_count DESC';
            break;
            case 2 /* Ending soonest */:
                $sql .= ' ORDER BY end_datetime DESC';
            break;
            case 3 /* Tickets Remaining DESC */:
                $sql .= ' ORDER BY sold_tickets ASC';
            break;
            case 4 /* Tickets Remaining ASC */:
                $sql .= ' ORDER BY sold_tickets DESC';
            break;
            case 5 /* Random */:
                $sql .= ' ORDER BY RAND()';
            break;
            case 0 /* Best odds (1 ticket) */:
            default:
                $sql .= ' ORDER BY competitions.end_datetime ASC, competitions.sold_tickets ASC';
            break;
        }

        if (isset($query['limit']) && intval($query['limit']) > 0) {
            $sql .= ' LIMIT '.intval($query['limit']);
        }

        return $wpdb->get_results($sql, 'ARRAY_A');
    }

    /**
     * Gets an array of competition ending dates after today
     */
    function db_get_competition_dates() {
        global $wpdb;
        $sql = "SELECT
                DATE_FORMAT(end_datetime, '%Y-%m-%d') as end_date,
                COUNT(1) as draw_count
                FROM
                ".$this->db_competitions_table_name." as competitions
                WHERE end_datetime > NOW()
                GROUP by end_date";

        return $wpdb->get_results($sql, 'OBJECT_K');
    }

    /**
     * Determine whether a string starts with a given string
     */
    function str_starts_with( $haystack, $needle ) {
         $length = strlen( $needle );
         return substr( $haystack, 0, $length ) === $needle;
    }

    function debug_log( $string, $object = NULL ) {
        $micro_date = microtime();
        $date_array = explode(" ",$micro_date);
        $date = date("Y-m-d H:i:s",$date_array[1]);

        echo '['.$date.'] ' . $string . ($object == NULL ? '' : ' ' . print_r($object,true)) . '<br/>';
    }

    /**
     * Formats a datetime string to the correct MySQL format
     */
    function format_mysql_date($string) {
        return date("Y-m-d H:i:s", strtotime($string));
    }
}

function run_raffle_wise_marketplace() {

	$GLOBALS['RaffleWiseMarketplace'] = new RaffleWiseMarketplace();
	$GLOBALS['RaffleWiseMarketplace']->run();

}
run_raffle_wise_marketplace();